Source: pypdf
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Scott Kitterman <scott@kitterman.com>,
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 dh-sequence-python3,
 flit,
 pybuild-plugin-pyproject,
 python3-all,
 python3-pil <!nocheck>,
 python3-pycryptodome <!nocheck>,
 python3-pytest <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-pytest-timeout <!nocheck>,
 python3-yaml,
Standards-Version: 4.7.0
Homepage: https://pypdf.readthedocs.io/en/latest/
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/pypdf.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pypdf
Testsuite: autopkgtest-pkg-pybuild

Package: python3-pypdf
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 python3-pil,
 python3-pycryptodome,
Description: Pure-Python library built as a PDF toolkit (Python 3)
 A Pure-Python library built as a PDF toolkit.  It is capable of:
   - extracting document information (title, author, ...),
   - splitting documents page by page,
   - merging documents page by page,
   - cropping pages,
   - merging multiple pages into a single page,
   - encrypting and decrypting PDF files.
 .
 By being Pure-Python, it should run on any Python platform without any
 dependencies on external libraries.  It can also work entirely on StringIO
 objects rather than file streams, allowing for PDF manipulation in memory.
 It is therefore a useful tool for websites that manage or manipulate PDFs.
 .
 This is the Python 3 version of the package.
